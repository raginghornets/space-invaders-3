/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders.scenes;

import engine.GameObject;
import engine.Graphics;
import engine.Vector2D;
import java.util.List;
import javafx.animation.AnimationTimer;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import spaceinvaders.assets.Invader;
import spaceinvaders.assets.Invaders;
import spaceinvaders.assets.MainEnvironment;
import spaceinvaders.assets.Ship;

/**
 *
 * @author Eric Nguyen
 */
public class MainScene {
    
  private MainEnvironment env = new MainEnvironment(800, 600);
  
  private Ship ship = env.ship;
  private Invaders invaders = env.invaders;
  private Invader bonusInvader = env.bonusInvader;
  private List<GameObject> shields = env.shields;

  private final int WIDTH = env.getWidth(), HEIGHT = env.getHeight();
  private final Color BACKGROUND = env.BACKGROUND;

  private final Stage STAGE = new Stage();
  private final StackPane LAYOUT = new StackPane();
  private final Scene SCENE = new Scene(LAYOUT, WIDTH, HEIGHT);
  private final Canvas CANVAS = new Canvas(SCENE.getWidth(), SCENE.getHeight());
  private final GraphicsContext CTX = CANVAS.getGraphicsContext2D();
  private final Graphics GFX = new Graphics(CANVAS);

  private final AnimationTimer LOOP;
  private static boolean loopRunning = true;

  public MainScene() {
    STAGE.setTitle("Space Invaders");
    STAGE.setOnCloseRequest(req -> System.exit(0));
    STAGE.setScene(SCENE);
    LAYOUT.getChildren().add(CANVAS);

    

    // Ship controls
    SCENE.setOnKeyPressed(e -> {
      switch(e.getCode()) {
        // Move left
        case LEFT:
        case A:
          if (ship.isAlive()) {
            ship.velocity = Vector2D.LEFT;
          }
          break;

        // Move right
        case RIGHT:
        case D:
          if(ship.isAlive()) {
            ship.velocity = Vector2D.RIGHT;
          }
          break;

        // Shoot
        case SPACE:
          if (ship.getProjectile().isIdle()
            || ship.getProjectile().position.y < 0) {
            ship.shoot();
          }
          break;
        
        // Retry
        case R:
          if (!ship.isAlive()) {
            env = new MainEnvironment(800, 600);
            ship = env.ship;
            invaders = env.invaders;
            bonusInvader = env.bonusInvader;
            shields = env.shields;
          }
          break;
          
        // Suicide
        case O:
          ship.die();
          break;
          
        // Pause/Resume game
        case ESCAPE:
          togglePause();
          break;

        default:
          break;
      }
    });

    SCENE.setOnKeyReleased(e -> {
      switch(e.getCode()) {

        // Stop moving
        case LEFT:
        case RIGHT:
        case A:
        case D:
          ship.velocity = Vector2D.ZERO;
          break;

        default:
          break;
      }
    });

    // Display the application window
    STAGE.show();

    Runnable ship_move = () -> {
      env.moveShip(ship);
    };

    Runnable ship_projectile_move = () -> {
      env.moveShipProjectile();
    };

    Runnable invaders_move = () -> {
      invaders.getInvaders().forEach((List<GameObject> row) -> {
        row.forEach((invader) -> {
          env.invaderMove((Invader) invader);
        });
      });
    };

    Runnable invaders_shoot = () -> {
      env.invaderShoot((Invader) invaders.getRandomBottomInvader());
    };

    Runnable invaders_projectiles_move = () -> {
      invaders.getInvaders().forEach((List<GameObject> row) -> {
        row.forEach((invader) -> {
          env.moveInvaderProjectile((Invader) invader);
        });
      });
    };

    Runnable bonus_invader_move = () -> {
      env.invaderMove(bonusInvader);
    };

    Runnable bonus_invader_shoot = () -> {
      env.invaderShoot(bonusInvader);
    };

    Runnable bonus_invader_projectile_move = () -> {
      env.moveInvaderProjectile(bonusInvader);
    };

    // The main loop which constantly updates the game
    this.LOOP = new AnimationTimer() {

      @Override
      public void handle(long now) {

        // Threads which run simultaneously
        (new Thread(ship_move)).start();
        (new Thread(ship_projectile_move)).start();
        (new Thread(invaders_move)).start();
        (new Thread(invaders_shoot)).start();
        (new Thread(invaders_projectiles_move)).start();

        // Start moving the bonus invader if score is high enough
        if (ship.getScore() > 500) {
          (new Thread(bonus_invader_move)).start();
          (new Thread(bonus_invader_shoot)).start();
          (new Thread(bonus_invader_projectile_move)).start();
        }

        // Graphics

        // --Background
        GFX.drawBackground(BACKGROUND);

        // --Score
        GFX.drawText("SCORE: " + ship.getScore(), 25, 25, Color.WHITE);

        // --Lives
        GFX.drawText("LIVES: ", WIDTH - WIDTH / 10 - 55, 25, Color.WHITE);
        for(int i = 0; i < ship.getLives(); i++) {
          CTX.drawImage(ship.image, WIDTH - WIDTH / 10 + i * ship.image.getWidth() + i * 5, 17.5f);
        }

        // --Ship
        GFX.drawGameObject(ship);
        GFX.drawGameObject(ship.getProjectile());
        
        if (!ship.isAlive()) {
          GFX.drawText("Press 'r' to retry.", WIDTH / 2 - 45, 25, Color.WHITE);
        }

        // --Invaders
        invaders.getInvaders().forEach((List<GameObject> row) -> {
          row.stream().map((invader) -> {
              GFX.drawGameObject(invader);
              return invader;
          }).forEachOrdered((invader) -> {
              GFX.drawGameObject(((Invader) invader).getProjectile());
          });
        });

        // --Bonus invader
        GFX.drawGameObject(bonusInvader);
        GFX.drawGameObject(bonusInvader.getProjectile());

        // --Shields
        shields.forEach((shield) -> {
          GFX.drawGameObject(shield);
        });
      }

    };

  }

  public void start() {        
    LOOP.start();
  }

  public void stop() {
    LOOP.stop();
  }

  public void togglePause() {
    if (loopRunning) {
      GFX.drawText("PAUSED", WIDTH / 2 - 25, HEIGHT / 2, Color.WHITE);

      LOOP.stop();
      loopRunning = false;
    } else {
      LOOP.start();
      loopRunning = true;
    }
  }
            
}