/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders.assets;

import engine.GameObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Eric Nguyen
 */
public class Invaders {
    
  private final List<List<GameObject>> invaders;
  private final int rows, cols;

  private final float spacing;

  public Invaders(float x, float y) {
    spacing = 25;
    invaders = new ArrayList<>();
    rows = 5;
    cols = 30;
    for(int r = 0; r < rows; r++) {
      ArrayList<GameObject> newRow = new ArrayList<>();
      
      // Set invader image based on row
      int type;
      switch(r) {
        case 0:
        case 1:
          type = 2;
          break;
        case 2:
        case 3:
          type = 1;
          break;
        default:
          type = 0;
          break;
      }
      
      // Initialize invaders and add them to the new row
      for(int c = 0; c < cols; c++) {
        GameObject newInvader = new Invader(type, c * spacing + x, r * spacing + y);
        newRow.add(newInvader);
      }
      
      invaders.add(newRow);
    }
  }

  public GameObject getRandomBottomInvader() {
    Random random = new Random();
    return getBottomInvaders().get(random.nextInt(invaders.get(invaders.size() - 1).size()));
  }

  public List<GameObject> getBottomInvaders() {
    List<GameObject> bottomInvaders = new ArrayList<>();
    for(int r = 0; r < invaders.size(); r++) {
      for(int c = 0; c < invaders.get(r).size(); c++) {
        if(invaders.get(r).get(c).equals(bottomInvader(c))) {
          bottomInvaders.add(invaders.get(r).get(c));
        } 
      }
    }
    return bottomInvaders;
  }

  public void changeDirection() {
    invaders.forEach((List<GameObject> row) -> {
      row.forEach((invader) -> {
        ((Invader) invader).changeDirection();
      });
    });
  }

  public void resetProjectiles() {
    getInvaders().forEach((List<GameObject> row) -> {
      row.forEach((invader) -> {
        ((Projectile) ((Invader) invader).getProjectile()).hide();
      });
    });
  }

  public List<List<GameObject>> getInvaders() {
    return invaders;
  }

  private GameObject bottomInvader(int column) {
    int row = invaders.size() - 1;
    GameObject bottom = invaders.get(row).get(column);
    while(row > 0 && !((Invader) bottom).isAlive()) {
      row--;
      bottom = invaders.get(row).get(column);
    }
    return bottom;
  }

  private boolean isBottom(GameObject invader) {
    return getBottomInvaders().contains(invader);
  }
           
}
