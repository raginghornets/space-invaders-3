/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders.assets;

import engine.GameObject;
import engine.Vector2D;
import java.util.Random;

/**
 *
 * @author Eric Nguyen
 */
public class Invader extends GameObject implements Character {
    
  private GameObject projectile;
  private boolean alive;
  private int value;
  private int type;

  public Invader(int type) {
    super("file:Sprites/Invader_" + type + "-0.png");

    this.type = type;
    value = 100;
    alive = true;
    velocity = Vector2D.RIGHT;
    movementSpeed = 0.25f;
    projectile = new Projectile("file:Sprites/Projectile_1.png");

    // Hide projectile
    ((Projectile) projectile).hide();
  }

  public Invader(int type, float x, float y) {
    this(type);
    position = new Vector2D(x, y);      
  }

  public void changeDirection() {
    velocity = velocity.multiply(-1);
    position = position.add(Vector2D.DOWN);
  }

  @Override
  public void shoot() {
    Random random = new Random();
    int rand = random.nextInt(2) + 1;
    GameObject projectileClone = new Projectile("file:Sprites/Projectile_" + rand + ".png");
    projectileClone.position = position.add(Vector2D.RIGHT.multiply(scale.x / 2 - projectileClone.scale.x / 2));
    projectileClone.velocity = Vector2D.DOWN.multiply(projectileClone.movementSpeed);
    projectile = projectileClone;
  }

  @Override
  public void die() {
    scale = Vector2D.ZERO;
    velocity = Vector2D.ZERO;
    alive = false;
  }

  @Override
  public GameObject getProjectile() {
    return projectile;
  }

  @Override
  public boolean isAlive() {
    return alive;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  public int getType() {
    return type;
  }

  /**
   * @param left mark for wrap
   * @param right mark for wrap
   */
  public void wrapScreenX(int left, int right) {
    if (position.x > right && velocity.equals(Vector2D.RIGHT)) {
      position = new Vector2D(left, position.y);
    } else if (position.x < left && velocity.equals(Vector2D.LEFT)) {
      position = new Vector2D(right, position.y);
    }
  }
  
}
