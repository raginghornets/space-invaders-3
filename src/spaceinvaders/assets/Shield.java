/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders.assets;

import engine.GameObject;
import engine.Vector2D;

/**
 *
 * @author Eric Nguyen
 */
public class Shield extends GameObject {
    
  private int hp;
  
  public Shield() {
    super("file:Sprites/Shield.png");
    hp = 10;
  }
  
  public Shield(float x, float y) {
    this();
    position = new Vector2D(x, y);
  }

  /**
   * Decrement hp and scale down 10%
   */
  public void hit() {
    if(hp > 0) {
      hp--;
      scale = scale.multiply(0.9f);
      position = position.add(scale.multiply(0.05f));
    } else {
      scale = Vector2D.ZERO;
    }
  }
}
