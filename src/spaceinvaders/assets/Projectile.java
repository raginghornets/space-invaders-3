/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders.assets;

import engine.GameObject;
import engine.Vector2D;

/**
 *
 * @author Eric Nguyen
 */
public class Projectile extends GameObject {
    
  public Projectile(String imgPath) {
    super(imgPath);
    movementSpeed = 2.5f;
  }
  
  public void hide() {
    position = new Vector2D(-10, -10);
  }
    
}
