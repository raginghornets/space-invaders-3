/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders.assets;

import engine.GameEnvironment;
import engine.GameObject;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.paint.Color;

/**
 * @author Eric Nguyen
 */
public class MainEnvironment extends GameEnvironment {
  public final Color BACKGROUND = Color.BLACK;

  public Ship ship;
  public Invaders invaders;
  public Invader bonusInvader;
  public List<GameObject> shields;

  public MainEnvironment(int w, int h) {
    super(w, h);
    
    // Ship
    ship = new Ship(super.getWidth() / 2, super.getHeight() - super.getHeight() / 25);
    ship.position.x -= ship.scale.x / 2;
    
    // Invaders
    invaders = new Invaders(25, 75);
    
    // Bonus invader
    bonusInvader = new Invader(3, -25, 50);
    bonusInvader.movementSpeed = 10;
    
    // Shields
    shields = new ArrayList<>();
    for(int i = 0; i < 15; i++) {
      shields.add(new Shield(42.5f + 50 * i, h - h / 8));
    }
  }

  public void moveShip(Ship ship) {
    if (inBounds(ship)) {
      ship.move();
    }
  }

  public void invaderMove(Invader invader) {
    // Move
    if (inBounds(invader) && !ship.getProjectile().colliding(invader)) {
      invader.move();
      
    // Destroy
    } else if (ship.getProjectile().colliding(invader)){
      invader.die();
      ship.addScore(100);
      ((Projectile) ship.getProjectile()).hide();
    
    // Change direction
    } else {
      if (invader.getType() < 3) {
        invaders.changeDirection();
      } else {
        invader.wrapScreenX(-75, 75);
      }
    }
  }

  public void invaderShoot(Invader invader) {
    if (invader.isAlive() && (invader.getProjectile().isIdle() || invader.getProjectile().position.y > height)) {
      invader.shoot();
    }
  }

  public void moveInvaderProjectile(Invader invader) {
    // Move
    if (!invader.getProjectile().colliding(ship) && !invader.getProjectile().colliding((List<GameObject>) shields)) {
      invader.getProjectile().move();
      
    // Hit GameObject
    } else {
      if (invader.getProjectile().colliding(ship)) {
        shipHit();
      } else if (invader.getProjectile().colliding((List<GameObject>) shields)) {
        ((Shield) invader.getProjectile().getColliding((List<GameObject>) shields)).hit();
      }
      ((Projectile) invader.getProjectile()).hide();
    } 
  }

  public void shipHit() {
    if (ship.isAlive()) {
      ship.hit();

      // Stop invaders from shooting after ship gets hit
      invaders.resetProjectiles();
      
      if (!ship.isAlive()) {
        ship.die();
      }
    }   
  }
  
  public void moveShipProjectile() {
    if (!ship.getProjectile().colliding((List<GameObject>) shields)) {
      ship.getProjectile().move();
    } else {
      ((Shield) ship.getProjectile().getColliding(shields)).hit();
      
      ((Projectile) ship.getProjectile()).hide();
    }
  }
}
