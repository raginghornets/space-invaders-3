/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

/**
 *
 * @author Eric Nguyen
 */
public class GameEnvironment {
    
  protected final int width, height;

  public GameEnvironment(int w, int h) {
    width = w;
    height = h;
  }

  public boolean inBounds(GameObject obj) {
    return (obj.position.x > 0 && obj.velocity.x < 0)
      || (obj.position.x + obj.scale.x < width && obj.velocity.x > 0)
      || (obj.position.y > 0 && obj.velocity.y < 0)
      || (obj.position.y + obj.scale.y < height && obj.velocity.y > 0)
      || (obj.velocity.equals(Vector2D.ZERO));
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }
}
