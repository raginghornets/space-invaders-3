/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author Eric Nguyen
 */
public class Graphics {
    
  private final GraphicsContext ctx;
  private final double width, height;

  public Graphics(Canvas canvas) {
    this.ctx = canvas.getGraphicsContext2D();
    width = canvas.getWidth();
    height = canvas.getHeight();
  }

  public void drawBackground(Color color) {
    ctx.setFill(color);
    ctx.fillRect(0, 0, width, height);
  }

  public void drawGameObject(GameObject obj) {
    ctx.drawImage(obj.image, obj.position.x, obj.position.y, obj.scale.x, obj.scale.y);
  }

  public void drawText(String text, float x, float y, Color color) {
    ctx.setStroke(color);
    ctx.strokeText(text, x, y);
  }
    
}
