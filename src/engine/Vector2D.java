/*
 * To ch nmange this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

/**
 *
 * @author Eric Nguyen
 */
public class Vector2D {
    
  public static final Vector2D
    LEFT = new Vector2D(-1, 0),
    RIGHT = new Vector2D(1, 0),
    UP = new Vector2D(0, -1),
    DOWN = new Vector2D(0, 1),
    ZERO = new Vector2D(0, 0);

  public float x, y;

  public Vector2D(float x, float y) {
    this.x = x;
    this.y = y;
  }

  /**
   * @param other
   * @return A new vector containing the sum of this vector's values and other vector's values
   */
  public Vector2D add(Vector2D other) {
    return new Vector2D(this.x + other.x, this.y + other.y);
  }

  /**
   * @param n
   * @return A new vector containing the product of this vector's values multiplied by n
   */
  public Vector2D multiply(float n) {
    return new Vector2D(this.x * n, this.y * n);
  }

  @Override
  public boolean equals(Object other) {
    if (other.getClass() != Vector2D.class) {
      return false;
    }
    return x == ((Vector2D) other).x && y == ((Vector2D) other).y;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 71 * hash + Float.floatToIntBits(this.x);
    hash = 71 * hash + Float.floatToIntBits(this.y);
    return hash;
  }

  @Override
  public String toString() {
    return "<" + x + ", " + y + ">";
  }
  
}
