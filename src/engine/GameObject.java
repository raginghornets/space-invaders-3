/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import java.util.List;
import javafx.scene.image.Image;

/**
 *
 * @author Eric Nguyen
 */
public abstract class GameObject {
    
  public Vector2D position, scale, velocity;
  public Image image;

  public float movementSpeed;

  public GameObject(String imgPath) {
    position = Vector2D.ZERO;
    velocity = Vector2D.ZERO;
    movementSpeed = 0;
    this.image = new Image(imgPath);
    scale = new Vector2D((int) image.getWidth(), (int) image.getHeight());
  }

  @Override  
  public String toString() {
    return "GameObject\n "
      + "position: " + position + "\n "
      + "scale: " + scale + "\n "
      + "velocity: " + velocity + "\n "
      + "movementSpeed: " + movementSpeed + "\n";
  }

  /**
   * Move this GameObject's position by its velocity multiplied by its movement speed. (position = velocity * movementSpeed)
   * <br>
   * If this GameObject's velocity is equal to Vector2D.ZERO [new Vector2D(0, 0)], it will not move.
   */
  public void move() {
    position = position.add(velocity.multiply(movementSpeed));
  }

  /**
   * @return true if velocity equals Vector2D.ZERO
   */
  public boolean isIdle() {
    return velocity.equals(Vector2D.ZERO);
  }

  /**
   * @param other
   * @return true if this is colliding with other
   */
  public boolean colliding(GameObject other) {
    return this.position.x >= other.position.x
      && this.position.x <= other.position.x + other.scale.x
      && this.position.y >= other.position.y
      && this.position.y <= other.position.y + other.scale.x;
  }

  /**
   * @param otherList
   * @return true if this is colliding with any GameObject in otherList
   */
  public boolean collidingList(List<List<GameObject>> otherList) {
    return otherList.stream().anyMatch((other) -> (colliding(other)));
  }

  /**
   * @param other
   * @return true if this is colliding with any GameObject in other
   */
  public boolean colliding(List<GameObject> other) {
    return other.stream().anyMatch((obj) -> (colliding(obj)));
  }
    
  /**
   * @param other
   * @return the GameObject in a list that this GameObject is currently colliding with
   */
  public GameObject getColliding(List<GameObject> other) {
    for (GameObject obj : other) {
      if (colliding(obj)) {
        return obj;
      }
    }
    return null;
  }
    
}